import * as React from 'react'
import styles from './Footer.module.scss'

const Footer = () => 
    <footer className={styles.footer}>
        <div className={styles.footerText}>
            © 2017 Even Financial, Inc - CONFIDENTIAL
        </div>
    </footer>

export default Footer