# Github Search Mini Project

## Tech
1. React /w TypeScript and Css Modules
2. Redux

## To run 
1. install dependencies - `yarn install`
2. start project - `yarn start`
3. Run test - `yarn test`

## Live Demo
https://evenfinancial-githubsearch.netlify.com/

## Third Party Dependencies
- https://www.npmjs.com/package/axios
- https://www.npmjs.com/package/antd - { Pagination }
- https://www.npmjs.com/package/react-redux
- https://www.npmjs.com/package/redux
---

